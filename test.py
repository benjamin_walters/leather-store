# Tests to see if cx_oracle is installed
try:
    import cx_Oracle
    print("Oracle Driver is installed. You're good to go!")
except ImportError:
    print("cx_Oracle is not installed properly :(")
    print("Make sure you followed all of the steps correctly")
