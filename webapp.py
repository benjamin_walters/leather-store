# Ernest Dogbe, Zhijun Zhao, Benjamin Walters
# This is the driving program for the leather store application
# This is a web server that interprets each url
# Each '@route' defines what to do when the user requests that url
# when you run this file (`python2.7 webapp.py`), this will run on localhost:8080
from bottle import get, post, request, static_file, route, run, error, redirect, response, template, SimpleTemplate
from database import *

# Retrieves columns name from cursor
def getHeaders(cursor):
	headers = []
	for tuple in cursor.description:
		headers.append(tuple[0])
	return headers

# home page
@route('/')
def page_index():
    return template('templates/index.tpl', tables = tables)

# Page that displays all orders for a given customer (by id)
@route('/query_orders', method="POST")
def query_orders():
	c_id = request.forms.get('customer_id')
	query = "SELECT O.order_id, O.order_date, O.order_total, O.order_status, O.billing_address_id, O.delivery_address_id"
	query += " FROM Orders O, customers C"
	query += " WHERE C.customer_id = O.customer_id AND c.customer_id = :c_id"
	# use prepare statement to protect
	conn = connect()
	cursor = conn.cursor()
	cursor.prepare(query)
	cursor.execute(None, {'c_id':c_id})
	headers = getHeaders(cursor)
	heading = "All orders for customer with id: " + str(c_id)
	return template('templates/query.tpl', cursor=cursor, heading=heading, headers=headers, editable=False)
	# ^ Cannot be editable	

# Page that displays last_purchase  for a given customer (by id)
@route('/query_derived', method="POST")
def query_derived():
	c_id = request.forms.get('customer_id')
	query = "SELECT c.last_purchase, TIME_INACTIVE(c.customer_id), c.first_name, c.last_name"
	query += " FROM customers c"
	query += " WHERE c.customer_id = :c_id"
	# use prepare statement to protect
	conn = connect()
	cursor = conn.cursor()
	cursor.prepare(query)
	cursor.execute(None, {'c_id':c_id})
	lastPurch = ""
	timeInact = ""
	for last_purchase,time_inactive,first,last in cursor:
		lName = last
		fName = first
		lastPurch = last_purchase
		timeInact = time_inactive

	return template('templates/derived.tpl', last_purchase=lastPurch, time_inactive=timeInact, first_name=fName, last_name=lName)



# Displays entire table - gets table_name from post
@route('/query', method="POST")
def query_post():
	table_name = request.forms.get('table_name')
	conn = connect()
	cursor = conn.cursor()
	# we have to validate table_name ourselves
	if not (table_name in tables):
		return template('templates/error.tpl', error=table_name+"is not a valid table name")
	cursor.execute("""
			SELECT * FROM """ + table_name)
	headers = getHeaders(cursor)
	heading = table_name.upper() + " table:"
	return template('templates/query.tpl', cursor=cursor, heading=heading, headers=headers, table_name=table_name)

# error
@route('/query')
def query():
	return template('templates/error.tpl', error="Invalid table name. Go home and try again")
	
# Receives columns, table, values - inserts new row
@route('/create_submit', method="POST")
def create_submit_post():
	tname = request.forms.get("table_name")
	values = []
	for c in schema[tname]:
		values.append(request.forms.get(c))
	colNames = schema[tname]
	
	query = "INSERT INTO " + tname + " ("
	first = True
	for name in colNames:
		if not first:
			query += ","
		else:
			first = False
		query += name
	query += ") VALUES ("
	
	c = 0
	first = True
	for val in values:
		if not first:
			query += ","
		else:
			first = False

		if colNames[c] == "last_purchase":
			query += "TO_TIMESTAMP('" + val + "', 'YYYY-MM-DD')"
		else:
			query += "'" + val + "'"
		c += 1
	query += ")"
	
	conn = connect()
	cursor = conn.cursor()
	try:
		cursor.execute(query)
		cursor.close()
		conn.commit()
		conn.close()
	except cx.DatabaseError as e:
		return template('templates/error.tpl', error=str(e))
	
	return template('templates/inserted.tpl', table_name=tname)
	
# Receives columns, table, values - edits row
@route('/edit_submit', method="POST")
def edit_submit_post():
	tname = request.forms.get("table_name")
	values = []
	colNames = []
	initCols = schema[tname]
	for c in range(1,len(initCols)):
		val = request.forms.get(initCols[c])
		if initCols[c] == "last_purchase":
			val = 'TO_TIMESTAMP(\'' + val + '\', \'YYYY-MM-DD\')'
		else:
			val = "'" + val + "'"
		values.append(val)  # column names are not upper case - coming from database.py
		colNames.append(initCols[c])

	keyName = request.forms.get("keyName")
	keyVal = request.forms.get("keyVal")     
	
	query = "UPDATE " + tname + " SET "
	for col,val in zip(colNames,values):
		query += col + "=" + val + ","

	query = query[:-1]  # removes the extra comma
	
	query += " WHERE " + keyName + " = " + keyVal

	conn = connect()
	cursor = conn.cursor()
	try:
		cursor.execute(query)
		cursor.close()
		conn.commit()
		conn.close()
	except cx.DatabaseError as e:
		return template('templates/error.tpl', error=str(e))

	return template('templates/edited.tpl', table_name=tname)
	

# Page to edit a row
@route('/edit', method="POST")
def edit_post():
	tname = request.forms.get("table_name")
	values = []
	colNames = schema[tname]
	for c in colNames:
		values.append(request.forms.get(c.upper()))  # column names are upper case on query page

	keyName = colNames[:1][0]  # first col name is key name - it is NOT removed from colNames
	keyVal = values[:1][0]     # same ^
	colNames = colNames[1:] # remove key
	values = values[1:]

	return template('templates/edit_form_post.tpl', table_name=tname, colNames=colNames, values=values, keyName=keyName, keyVal=keyVal)

# Submits delete query 
# Query input is already validated in input part
@route('/delete_submit', method="POST")
def delete_post():
	tname = request.forms.get("table_name")
	keyName = request.forms.get("keyName")
	keyVal = request.forms.get("keyVal")

	query = "DELETE FROM " + tname
	query += " WHERE "
	query += keyName + " = '" + keyVal + "'"
	
	conn = connect()
	cursor = conn.cursor()
	try:
		cursor.execute(query)
		cursor.close()
		conn.commit()
		conn.close()
	except cx.DatabaseError as e:
		return template('templates/error.tpl', error=str(e))
	
	return template('templates/deleted.tpl', table_name=tname)

# Form to create new row
@route('/create', method="POST")
def create_form_post():
	tname = request.forms.get("table_name")
	return template('templates/create_form_post.tpl', tables=tables, table_name=tname, schema=schema)

# Lets you pick a table to add a row to
@route('/create')
def create_form():
	return template('templates/create_form.tpl', tables=tables)
	
# Retrieves stylesheets
@route('/styles/<name>')
def style(name):
	return static_file(name, root="./stylesheets/")


# Starts local server on 8080
run(host='localhost', port=8080, debug=True, reloader=True)