import cx_Oracle as cx

hostname = 'fourier.cs.iit.edu'
port = 1521
SID = 'orcl'
global dsn_tns 
dsn_tns = cx.makedsn(hostname, port, SID)
global user
user = 'bwalter4'
global password
password = 'leather21'

# only hard coded so that we don't have to use a bunch of extra database calls
# don't have time to integrate AJAX
# dict where key is table name - entry is list of columns
global schema
schema = {
	"address":["address_id","street","state","zip_code","country","city"],
	"category":["category_id","category_name","category_image","parent_id","description"],
	"colors":["color_id","color","product_id"],
	"customers":["customer_id","first_name","middle_name","last_name","password","home_phone","cell_phone","email","last_purchase","gender","address_id"],
	"item_details":["item_details_id","price","quantity","color","order_id","product_id"],
	"manufacturers":["manufacturer_id","name","logo_url","description","title"],
	"orders":["order_id","order_date","order_total","order_status","customer_id","billing_address_id","delivery_address_id"],
	"products":["product_id","product_name","description","price","weight","category_id","manufacturer_id","model","image_location","status"]
}
# "students":["id","first_name","last_name","credits","age"] -- was for testing purposes

# List of tables
global tables
tables = schema.keys()

# connects to database
def connect():
	return cx.connect(user=user, password=password,dsn=dsn_tns)
