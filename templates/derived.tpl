% include('templates/header.tpl', title='Add to Table')

<p>Information for {{last_name}}, {{first_name}}:<br />
Last purchase: {{last_purchase}}<br />
Time inactive (in days): {{time_inactive}}<br />
</p>

% include('templates/footer.tpl')