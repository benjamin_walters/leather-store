% include('templates/header.tpl', title='Add to Table')

New row inserted succesfully!

<form method="post" action="/create">
<input type="hidden" name="table_name" value={{table_name}} />
<input type="submit" value="Insert another row" />
</form>

<form method="post" action="/query">
<input type="hidden" name="table_name" value={{table_name}} />
<input type="submit" value="View {{table_name}} table" />
</form>

% include('templates/footer.tpl')