% include('templates/header.tpl', title='Add to Table')
% # requires an arr 'tables'
% columns = schema[table_name]

<p>Create a new row in the <strong>{{table_name}}</strong> table</p>

<!-- Form to add a new row to a given table -->
<form method="post" action="/create_submit">
	<table class="invisible">
		% for col in columns:
			<tr>
				% if col == "last_purchase":
					<td>{{col + " (YYYY-MM-DD)"}}: </td>
				% else:
					<td>{{col}}: </td>
				% end
				<td><input type="text" name="{{col}}" /></td>
			</tr>
		% end
	</table>
	<input type="hidden" name="table_name" value="{{table_name}}" />
	<input type="submit" value="Create new {{table_name}}" />
</form>

<form method="post" action="/create">
Or pick a different table: 
% include('templates/dropdown.tpl',options=tables, name='table_name', selected=table_name)
<input type="submit" value="Submit" />
</form><br />
% include('templates/footer.tpl')