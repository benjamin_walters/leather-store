%# Index (home page) for web app
% include('templates/header.tpl',title='Home')

<!-- List of reports that can be run -->
<h3>Reports</h3>

<!-- display entire table - i.e. SELECT * -->
<form action="/query" method="post">
Display entire table: 
%# Inserts a dropdown of table names
% include('templates/dropdown.tpl',options=tables, name='table_name')
<input type="Submit" value="Submit" />
</form>

<!-- Display all orders for a customer (by id) -->
<form action="/query_orders" method="post">
Display all orders for customer with id: 
<!-- input customer id -->
<input type="text" name="customer_id" />
<input type="Submit" value="Submit" />
</form>

<!-- Display time inactive and last purchase for a customer (by id) -->
<form action="/query_derived" method="post">
Display time inactive (derived attribute) for customer with id: 
<!-- input customer id -->
<input type="text" name="customer_id" />
<input type="Submit" value="Submit" />
</form>

<!-- List of creation stuff -->
<h3>Create</h3>

<form method="post" action="/create">
Add a new row to a table: 
% include('templates/dropdown.tpl',options=tables, name='table_name')
<input type="submit" value="Submit" />
</form>

</table>
% include('templates/footer.tpl')