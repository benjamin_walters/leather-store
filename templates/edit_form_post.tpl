% include('templates/header.tpl', title='Add to Table')
% # requires an arr 'tables'
% # requires keyName, keyVal, values, colNames

<!-- Like the create form - but pre loaded with the current values -->

<p>Edit row in the <strong>{{table_name}}</strong> table</p>

<!-- Form to edit a row in a given table -->
<form method="post" action="/edit_submit">
	<table class="invisible">
		<tr>
			<td>{{keyName}}: </td>
			<td>{{keyVal}} </td>
		</tr>
		% for col, val in zip(colNames,values):
			<tr>
				% if col == "last_purchase":
					<td>{{col + " (YYYY-MM-DD)"}}: </td>
				% else:
					<td>{{col}}: </td>
				% end
				
				<td><input type="text" name="{{col}}" value="{{val}}" /></td>
			</tr>
		% end
	</table>
	<input type="hidden" name="table_name" value="{{table_name}}" />
	<input type="hidden" name="keyName" value="{{keyName}}" />
	<input type="hidden" name="keyVal" value="{{keyVal}}" />
	<input type="submit" value="Edit row in {{table_name}}" />
</form>

<!-- form for a delete button -->
<form method="post" action="/delete_submit">
	<input type="hidden" name="table_name" value="{{table_name}}" />
	<input type="hidden" name="keyName" value="{{keyName}}" />
	<input type="hidden" name="keyVal" value="{{keyVal}}" />
	<input type="submit" value="Delete Row" />
</form>

% include('templates/footer.tpl')