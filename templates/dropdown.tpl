<!-- returns a drop down based on the options arr -->
% setdefault('selected', '')
<select name='{{name}}'>
	% for opt in options:
		% 	sel = ""
		%	if selected == opt:
		%		sel = " selected"
		%	end
		<option value="{{opt}}"{{sel}}>{{opt}}</option>
	% end
</select>