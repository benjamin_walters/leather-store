% include('templates/header.tpl', title='Query')
% setdefault('table_name', '<some_table>')
% setdefault('editable', True)
% setdefault('heading', "")


<p>{{heading}}</p>
<table>
<tr>
% if editable:
	<td class="query"></td>
% end
% for name in headers:
	<td class="query">{{name}}</td>
% end
</tr>
% for tuple in cursor:
	<tr>
		% if editable:
			<form action="/edit" method="post">
			<td class="query"><input type="submit" value="Edit/Delete"></td>
			<input type="hidden" name="table_name" value="{{table_name}}" />
		% end
		% ctr = 0
		% for item in tuple:
			<td class="query">{{item}}</td>
			% if editable:
				<input type="hidden" name="{{headers[ctr]}}" value="{{item}}" />
			% end
			% ctr += 1
		% end
		% if editable:
			</form>
		% end
	</tr>
% end
</table>

% include('templates/footer.tpl')